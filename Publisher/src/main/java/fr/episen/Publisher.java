package fr.episen;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.HashMap;
import java.util.Map;

public class Publisher {

    public static void main(String[] args) throws InterruptedException{
        SparkConf sparkConf = new SparkConf().setAppName("Publisher").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        SparkSession spark = SparkSession.builder().appName("Publisher").getOrCreate();

        String csvFilePath = "data.csv";
        String kafkaBootstrapServers = "localhost:9092";
        String kafkaTopic = "sujet11";

        // Lire le fichier CSV et stocker les données dans un DataFrame
        Dataset<Row> csvDataFrame = readCsvFile(spark, csvFilePath);

        // Afficher le contenu du DataFrame
        System.out.println("DataFrame from CSV :");
        csvDataFrame.show();

        // Afficher le schéma du DataFrame
        System.out.println("DataFrame Schema:");
        csvDataFrame.printSchema();

        // Envoie du DataFrame dans un topic Kafka
        writeToKafka(csvDataFrame, kafkaBootstrapServers, kafkaTopic);


        Thread.sleep(5000); // Attendre 5 secondes pour afficher les résultats avant de fermer
        sc.stop();
    }

    private static Dataset<Row> readCsvFile(SparkSession spark, String filePath) {
        return spark.read().option("header", "true").csv(filePath);
    }

    private static void writeToKafka(Dataset<Row> dataFrame, String bootstrapServers, String topic) {
        dataFrame.toJavaRDD().foreachPartition(partition -> {
            KafkaProducerFactory kafkaProducerFactory = KafkaProducerFactory.getInstance(bootstrapServers);

            partition.forEachRemaining(row -> {
                String message = row.mkString(",");
                kafkaProducerFactory.send(topic, message);
            });
        });
    }

    static class KafkaProducerFactory {
        private static KafkaProducerFactory instance;
        private final String bootstrapServers;
        private transient KafkaProducer<String, String> producer;

        private KafkaProducerFactory(String bootstrapServers) {
            this.bootstrapServers = bootstrapServers;
        }

        public static synchronized KafkaProducerFactory getInstance(String bootstrapServers) {
            if (instance == null) {
                instance = new KafkaProducerFactory(bootstrapServers);
            }
            return instance;
        }

        public KafkaProducer<String, String> getProducer() {
            if (producer == null) {
                Map<String, Object> props = new HashMap<>();
                props.put("bootstrap.servers", bootstrapServers);
                props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
                props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
                producer = new KafkaProducer<>(props);
            }
            return producer;
        }

        public void send(String topic, String message) {
            getProducer().send(new org.apache.kafka.clients.producer.ProducerRecord<>(topic, message));
        }
    }
}