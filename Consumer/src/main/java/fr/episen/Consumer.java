package fr.episen;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Consumer {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private static Connection connection;

    public static void  main(String[] args) {
        System.out.println("Waiting data from topic sujet11");
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "consumerGroup");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        SparkConf sparkConf = new SparkConf().setAppName("Consumer").setMaster("local[*]");

        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkConf, new Duration(50000));

        Collection<String> topics = Collections.singletonList("sujet11");

        // Initialiser la connexion PostgreSQL
        initializePostgresConnection();

        JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        streamingContext,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
                );

        stream.foreachRDD(rdd -> {
            rdd.foreach(record -> {
                System.out.println("Key: " + record.key() + ", Value: " + record.value());

                // Insérer les données dans PostgreSQL
                insertIntoPostgres(record.value());
            });
        });

        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            // Fermer la connexion à la fin de l'application Spark
            closePostgresConnection();
        }
    }

    private static void initializePostgresConnection() {
        try {
            System.out.println("Connecting to PostgreSQL database...");

            String jdbcUrl = "jdbc:postgresql://localhost:5432/bigdata";
            String jdbcUsername = "postgres";
            String jdbcPassword = "postgres";

            connection = DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);

            if (connection != null) {
                System.out.println("Connected to the PostgreSQL database.");
            } else {
                System.out.println("Failed to connect to the PostgreSQL database.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertIntoPostgres(String data) {
        try {
            if (connection != null) {
                // Colonnes dans le même ordre que dans le message Kafka
                String[] columns = data.split(",");

                // Construire la requête SQL il faut refaire ça parce que ça ne prend pas les colonnes
                StringBuilder queryBuilder = new StringBuilder("INSERT INTO infra (");
                for (int i = 0; i < columns.length; i++) {
                    queryBuilder.append(columns[i]);
                    if (i < columns.length - 1) {
                        queryBuilder.append(", ");
                    }
                }
                queryBuilder.append(") VALUES (");
                for (int i = 0; i < columns.length; i++) {
                    queryBuilder.append("?");
                    if (i < columns.length - 1) {
                        queryBuilder.append(", ");
                    }
                }
                queryBuilder.append(")");

                // Préparer le statement
                try (PreparedStatement preparedStatement = connection.prepareStatement(queryBuilder.toString())) {
                    // Remplir les valeurs dans le PreparedStatement
                    String[] values = data.split(",");
                    for (int i = 0; i < values.length; i++) {
                        preparedStatement.setString(i + 1, values[i].trim());
                    }

                    // Exécuter la mise à jour
                    preparedStatement.executeUpdate();
                }

                logger.info("Data inserted into PostgreSQL.");
            } else {
                logger.error("Connection to PostgreSQL is null.");
            }
        } catch (SQLException e) {
            logger.error("Error inserting data into PostgreSQL.", e);
        }
    }

    private static void closePostgresConnection() {
        try {
            if (connection != null) {
                connection.close();
                System.out.println("Closed PostgreSQL connection.");
            } else {
                System.out.println("Connection to PostgreSQL is already null.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
